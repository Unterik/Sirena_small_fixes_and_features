---
title: "Вики"
menu: "main"
weight: 2
---

# База знаний

- [📗 Правила сервера](/server_rules) - действующие правила игрового сервера Sirena.
- [👮 Корпоративный Закон](/corporate_law) (КЗ) - свод требований корпорации Nanotrasen (NT) ко всем разумным существам на территории объектов корпорации.
- [📒 Стандартные Рабочие Процедуры](https://station14.ru/wiki/Стандартные_Рабочие_Процедуры) (СРП) - набор руководящих принципов, которым необходимо следовать в соответствующих им обстоятельствах. Используется СРП от [Corvax](https://station14.ru).
- [🗺️ Map Viewer 14](https://maps.station14.ru/) - просмотрщик карт станций игры от [Corvax](https://station14.ru).
- [💽 Репозиторий](https://codeberg.org/Sirena/SS14-Sirena) - хранилище кода сборки Sirena. Является форком [репозитория Corvax'а](https://github.com/space-syndicate/space-station-14/).
- [📚 Space Station 14 RU Wiki](https://station14.ru/) - основной информационный ресурс об игре на русском языке от [Corvax](https://station14.ru).
