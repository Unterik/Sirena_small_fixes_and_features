---
title: "Главная"
menu: "main"
weight: 1
---

{{< logo "/images/logo.png" >}}

Sirena - сообщество одного из серверов игры [Space Station 14](https://spacestation14.io/) - ремейка [Space Station 13](https://ru.wikipedia.org/wiki/Space_Station_13) на собственном движке, написанном на C#, с открытым исходным кодом. Сервер ориентирован для высокого уровня отыгрыша ролей (HRP), поэтому рекомендуется прежде поиграть на других серверах, чтобы набрать опыт, и предназначен только для взрослой аудитории (🔞)!

IP-адрес: `{{< game_server_address >}}` ([как подключиться?](/connecting))

[🗣️ Discord](https://discord.gg/a87nCg3bHD) | [💸 Boosty](https://boosty.to/ss14-sirena) | [💾 Codeberg](https://codeberg.org/NekoDar/SS14-Sirena) | [Обжаловать 5.Е](https://forms.gle/jdLkr7vxWUGTbU4Z6) 

Состояние обновления:[![status-badge](https://ci.codeberg.org/api/badges/Sirena/Sirena_website/status.svg)](https://ci.codeberg.org/Sirena/Sirena_website)