---
title: "Подключение к серверу"
---

Прежде всего нужно установить игру из [Steam](https://store.steampowered.com/app/1255460/Space_Station_14/) или с [официального сайта](https://spacestation14.io/about/nightlies/).

Запустив игру, высветится меню входа в аккаунт, если у вас его нет, то зарегистрируйте его нажав на соответствующую кнопку:

![Login view](/images/connecting/login.png)

После регистрации нужно войти в этот аккаунт через лаунчер и увидеть это:

![Favourite Servers List](/images/connecting/favourites-empty.png)

Нажатием на кнопку "Add Favorite" в открывшемся окне добавляем наш сервер в избранное, вводя соответственно имя `Sirena` (может быть любое, используется только в лаунчере для удобства) и адрес `{{< game_server_address >}}` и нажимая кнопку "Add":

![Add Favourite Server](/images/connecting/add-favourite-server.png)

Теперь в списке избранных серверов появилась Sirena. Нажав на кнопку "Connect", начать подключение:

![Favourites Servers List with Sirena](/images/connecting/favourites-with-sirena.png)

Теперь можно прочитать [правила](/server_rules) и начинать играть!

---

Почему сервера Sirena нет в общем списке серверов? Сообщество стремится достичь качественного и высокого уровня отыгрыша ролей, поэтому сервер скрыт из хаба для уменьшения потока *залётных* игроков, нарушающих все правила подряд.
