# Sirena_website

Данный репозиторий создан для [сайта](http://sirena.gramsk.ru) от [репозитория](https://codeberg.org/NekoDar/SS14-Sirena)

# Куда жаловаться?
Жалобы и предложения можно оставить или на [discord](https://discord.gg/a87nCg3bHD) сервере, или по почте: sirena.server@mail.ru

# Состояние выгрузки файлов
[![status-badge](https://ci.codeberg.org/api/badges/Sirena/Sirena_website/status.svg)](https://ci.codeberg.org/Sirena/Sirena_website)